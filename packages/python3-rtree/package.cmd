::--------- Package settings --------
:: package name
set P=python3-rtree
:: version
set V=0.8.3
:: package version
set B=1
set HERE=%CD%

::--------- Prepare the environment
call ..\__inc__\prepare_env.bat %1
set OSGEO4W_ROOT=c:\osgeo4w64
set PATH=%OSGEO4W_ROOT%\bin;%PATH%

:: python3 package
call %OSGEO4W_ROOT%\bin\py3_env.bat

pip install rtree==%V%

sed -i s/spatialindex_c.dll/spatialindex_c-64.dll/g %PYTHONHOME%/Lib/site-packages/rtree/core.py


tar -C %OSGEO4W_ROOT% -cvjf %PKG_BIN% apps/Python37/Lib/site-packages/rtree

::--------- Installation
scp %PKG_BIN% %R%
cd %HERE%
scp setup.hint %R%
