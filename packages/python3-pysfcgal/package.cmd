::--------- Package settings --------
:: package name
set P=python3-pysfcgal
:: version
set V=0.1
:: package version
set B=1
:: pip name
set N=PySFCGAL

:: pip lowercase name
set L=PySFCGAL

set HERE=%CD%

::--------- Prepare the environment
call ..\__inc__\prepare_env.bat %1
set OSGEO4W_ROOT=c:\osgeo4w64
set PATH=%OSGEO4W_ROOT%\bin;%PATH%

:: python3 package
call %OSGEO4W_ROOT%\bin\py3_env.bat || goto :error

python3 -m pip install %L%==%V% || goto :error

cd %HERE%

c:\cygwin64\bin\tar -C %OSGEO4W_ROOT% -cvjf %PKG_BIN% ^
  apps/Python37/Lib/site-packages/%L% ^
  apps/Python37/Lib/site-packages/%N%-%V%.dist-info || goto :error

::--------- Installation
scp %PKG_BIN% %R% || goto :error
scp setup.hint %R% || goto :error
goto :EOF


:error
echo Build failed
exit /b 1
