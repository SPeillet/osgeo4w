::--------- Package settings --------
:: package name
set P=python-tempus-loader
:: version
set V=2.0.0
:: package version
set B=1
:: building dependencies
set BUILD_DEPS=python3-core setuptools python3-pglite

::--------- Prepare the environment
call ..\__inc__\prepare_env.bat %1

set OSGEO4W_ROOT=C:\osgeo4w64
call C:\OSGeo4W64\bin\py3_env.bat
set PATH=%PATH%;c:\osgeo4w64\bin;c:\cygwin64\bin

if "%1"=="test" (
wget --progress=bar:force https://gitlab.com/tempus-projects/tempus_loader/repository/master/archive.tar.bz2 -O tempus_loader.tar.bz2 || goto :error
) else (
wget --progress=bar:force https://gitlab.com/tempus-projects/tempus_loader/repository/archive.tar.bz2?ref=v%V% -O tempus_loader.tar.bz2 || goto :error
)
tar xjf tempus_loader.tar.bz2
cd tempus_loader-*
pip install --upgrade certifi
python setup.py install || goto :error

cd %HERE%

:: binary archive
tar -C c:\OSGeo4W64 -cjvf %PKG_BIN% apps/Python37/lib/site-packages/tempus_loader-%V%-py3.7.egg ^
  apps/Python37/Scripts/tempus_loader.exe ^
  apps/Python37/Scripts/tempus_loader-script.py || goto :error

:: source archive
tar -C %HERE% --transform 's,^,osgeo4w/,' -cvjf %PKG_SRC% package.cmd setup.hint || goto :error

::--------- Installation
call %HERE%\..\__inc__\install_archives.bat || goto :error

goto :EOF

:error
echo Build failed
exit /b 1
