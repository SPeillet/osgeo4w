::--------- Package settings --------
:: package name
set P=python3-pywin32-unofficial
:: version
set V=228
:: package version
set B=3

set HERE=%CD%

::--------- Prepare the environment
call ..\__inc__\prepare_env.bat %1
set OSGEO4W_ROOT=c:\osgeo4w64
call %OSGEO4W_ROOT%\bin\py3_env.bat
set PATH=%OSGEO4W_ROOT%\bin;%PATH%
set SITEPACKAGES=apps/Python37/Lib/site-packages

:: python3 package
call %OSGEO4W_ROOT%\bin\py3_env.bat

pip install pywin32==%V% || goto: error

copy %OSGEO4W_ROOT%\apps\Python37\Lib\site-packages\pywin32_system32\pythoncom37.dll %OSGEO4W_ROOT%\apps\Python37\DLLs\pythoncom37.dll || goto: error
copy %OSGEO4W_ROOT%\apps\Python37\Lib\site-packages\pywin32_system32\pywintypes37.dll %OSGEO4W_ROOT%\apps\Python37\DLLs\pywintypes37.dll || goto: error
cd %HERE%

tar -C %OSGEO4W_ROOT% -cvjf %PKG_BIN% ^
	apps/Python37/DLLs ^
	%SITEPACKAGES%/pythoncom.py ^
	%SITEPACKAGES%/pywin32.pth ^
	%SITEPACKAGES%/pywin32.version.txt ^
	%SITEPACKAGES%/pywin32-%V%.dist-info ^
	%SITEPACKAGES%/pythonwin ^
	%SITEPACKAGES%/isapi ^
	%SITEPACKAGES%/adodbapi ^
	%SITEPACKAGES%/win32comext ^
	%SITEPACKAGES%/win32com ^
	%SITEPACKAGES%/win32 || goto: error



::--------- Installation
scp %PKG_BIN% %R%
cd %HERE%
scp setup.hint %R%
