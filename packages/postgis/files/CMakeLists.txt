################################################################################
# CMakeLists.txt - root CMake configuration file, part PostGIS project
#
# Copyright (C) 2012 Mateusz Loskot <mateusz@loskot.net>
################################################################################
cmake_minimum_required(VERSION 3.4.0 FATAL_ERROR)
project(PostGIS)

################################################################################
# User options to control PostGIS features

option(POSTGIS_WITH_RASTER
    "Set ON|OFF (default=OFF) to control building raster extension" OFF)

option(POSTGIS_WITH_TOPOLOGY
    "Set ON|OFF (default=OFF) to control building topology extension" OFF)
    
option(POSTGIS_ENABLE_STATS
    "Set ON|OFF (default=ON) to control use ANALYZE statistics" ON)
set(POSTGIS_USE_STATS 1)
if(NOT POSTGIS_ENABLE_STATS)
    set(POSTGIS_USE_STATS 0)
endif()

option(POSTGIS_ENABLE_AUTOCACHE_BBOX
    "Set ON|OFF (default=ON) to control BBOX caching within geometries" ON)
set(POSTGIS_AUTOCACHE_BBOX 1)
if(NOT POSTGIS_ENABLE_AUTOCACHE_BBOX)
    set(POSTGIS_AUTOCACHE_BBOX 0)
endif()

option(POSTGIS_ENABLE_PROFILE
  "Set ON|OFF (default=OFF) to control use GEOS profiling" OFF)
set(POSTGIS_PROFILE 0)
if(POSTGIS_ENABLE_PROFILE)
    set(POSTGIS_PROFILE 1)
endif()

option(POSTGIS_ENABLE_DEBUG
  "Set ON|OFF (default=OFF) to control verbose debugging messages" OFF)
set(POSTGIS_DEBUG_LEVEL 0)
if(POSTGIS_ENABLE_DEBUG)
    set(POSTGIS_DEBUG_LEVEL 4)
endif()
add_definitions(-DPOSTGIS_DEBUG_LEVEL=${POSTGIS_DEBUG_LEVEL})

if (CMAKE_COMPILER_IS_GNUCC)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fPIC -Wall")
    set(LIBMFLAG "m")
    add_definitions(-DHAVE_GNU_ISFINITE)
elseif (MSVC)
    add_definitions(/DM_PI=3.14159265358979323846)
    add_definitions(/DM_PI_2=1.57079632679489661923)
	set (CMAKE_SHARED_LINKER_FLAGS "/ignore:4197")
endif()

#include(CheckFunctionExists)
#check_function_exists(finite HAVE_FINITE)
#message("have finite ${HAVE_FINITE}")
#check_function_exists(wtftoto HAVE_WTF)
#message("have wtf ${HAVE_WTF}")

################################################################################
# Path to additional CMake modules
set(CMAKE_MODULE_PATH ${PostGIS_SOURCE_DIR}/cmake ${CMAKE_MODULE_PATH})
set(CMAKE_MODULE_PATH ${PostGIS_SOURCE_DIR}/cmake/modules ${CMAKE_MODULE_PATH})

include(PostGISUtilities)

message(STATUS)
colormsg(_HIBLUE_ "Configuring PostGIS:")
message(STATUS)



################################################################################
# Dependency (required): PostgreSQL
#OLD: find_program(PG_CONFIG pg_config)
#if(NOT PG_CONFIG)
#    message(FATAL ERROR " Please check your PostgreSQL installation    ") 
#endif(NOT PG_CONFIG)
# TODO: we need both server library (postgres) and front end (pq)
find_package(PostgreSQL) # TODO: req version?
if(NOT PG_CONFIG)
    message(FATAL ERROR " Please check your PostgreSQL installation")
endif(NOT PG_CONFIG)

exec_program(${PG_CONFIG} ARGS --version OUTPUT_VARIABLE _PGSQL_VERSION_OUTPUT)
string(REGEX MATCHALL
    "[0-9]+"
    PGSQL_VERSION "${_PGSQL_VERSION_OUTPUT}")
unset(_PGSQL_VERSION_OUTPUT)

if(PGSQL_VERSION)
    string(REGEX REPLACE
        "([0-9]+);([0-9]);*[0-9]*" "\\1"
        PGSQL_VERSION_MAJOR "${PGSQL_VERSION}")
	if (${PGSQL_VERSION_MAJOR} LESS 10)
    string(REGEX REPLACE
        "([0-9]+);([0-9]);*[0-9]*" "\\2"
        PGSQL_VERSION_MINOR "${PGSQL_VERSION}")
	else (${PGSQL_VERSION_MAJOR} LESS 10)
		set(PGSQL_VERSION_MINOR 0)
	endif(${PGSQL_VERSION_MAJOR} LESS 10)
	set(POSTGIS_PGSQL_VERSION ${PGSQL_VERSION_MAJOR}${PGSQL_VERSION_MINOR})
	set(POSTGIS_PGSQL_HR_VERSION ${PGSQL_VERSION_MAJOR}.${PGSQL_VERSION_MINOR})
endif()


exec_program(${PG_CONFIG} ARGS --includedir
    OUTPUT_VARIABLE PGSQL_INCLUDEDIR)
exec_program(${PG_CONFIG} ARGS --includedir-server
    OUTPUT_VARIABLE PGSQL_INCLUDEDIR_SERVER)

include_directories(${PGSQL_INCLUDEDIR})
include_directories(${PGSQL_INCLUDEDIR_SERVER})
if(WIN32)
    include_directories(${PGSQL_INCLUDEDIR_SERVER}/port/win32)
    if(MSVC)    
        include_directories(${PGSQL_INCLUDEDIR_SERVER}/port/win32_msvc)
    endif()
endif()

exec_program(${PG_CONFIG} ARGS --pkglibdir OUTPUT_VARIABLE PGSQL_PKGLIBDIR)
exec_program(${PG_CONFIG} ARGS --sharedir OUTPUT_VARIABLE PGSQL_SHAREDIR)

################################################################################
# Dependency (required): PROJ.4
find_package(PROJ4)
if(NOT PROJ4_FOUND)
    message(FATAL ERROR " Please check your PROJ.4 installation") 
endif()

set(POSTGIS_PROJ_VERSION 62) # TODO
include_directories(${PROJ4_INCLUDE_DIR})

################################################################################
# Dependency (required): GEOS
find_package(GEOS)
if(NOT GEOS_FOUND)
    message(FATAL ERROR " Please check your GEOS installation") 
else()
endif()
#add_definitions(-DGEOS_DLL_IMPORT)

set(POSTGIS_GEOS_VERSION ${GEOS_VERSION_MAJOR}${GEOS_VERSION_MINOR}) # TODO
include_directories(${GEOS_INCLUDE_DIR})

if(POSTGIS_WITH_TOPOLOGY)
    #TODO: GEOS version >= 3.3.2.
    postgis_report_value(POSTGIS_WITH_TOPOLOGY)
endif()

################################################################################
# Dependency (required): LIBXML2
find_package(LibXml2)
if(NOT LIBXML2_FOUND)
    message(FATAL ERROR " Please check your LibXml2 installation") 
endif()

string(REGEX REPLACE "([0-9]+)\\.([0-9]+)\\..*" "\\1\\2" POSTGIS_LIBXML2_VERSION ${LIBXML2_VERSION_STRING})
include_directories(${LIBXML2_INCLUDE_DIR})

################################################################################
# Dependency (required): GEOS
#find_package(JSONC)
#if(NOT JSONC_FOUND)
#    message(FATAL ERROR " Please check your json-c installation") 
#endif()

################################################################################
# Dependency (optional): GDAL
if(POSTGIS_WITH_RASTER)
    find_package(GDAL)
endif()


################################################################################
# Dependency (required): ICONV
find_package(Iconv)
if(NOT ICONV_FOUND)
    message(FATAL ERROR " Please check your iconv installation") 
endif()
include_directories(${ICONV_INCLUDE_DIR})



################################################################################
# Version and build date information

# Extract version details from Version.config
if(EXISTS "${PostGIS_SOURCE_DIR}/Version.config")
  file(READ "${PostGIS_SOURCE_DIR}/Version.config" _POSTGIS_VERSION_CONFIG)
  string(STRIP ${_POSTGIS_VERSION_CONFIG} _POSTGIS_VERSION_CONFIG)

  string(REGEX REPLACE ".*POSTGIS_MAJOR_VERSION=([0-9]+)[\r\n\t\ ].*" "\\1"
    POSTGIS_MAJOR_VERSION ${_POSTGIS_VERSION_CONFIG})
  string(REGEX REPLACE ".*POSTGIS_MINOR_VERSION=([0-9]+)[\r\n\t\ ].*" "\\1"
    POSTGIS_MINOR_VERSION "${_POSTGIS_VERSION_CONFIG}")
  string(REGEX REPLACE ".*POSTGIS_MICRO_VERSION=([0-9]+[a-zA-Z]?[a-zA-Z]?[a-zA-Z]?)[\r\n\t ].*" "\\1"
    POSTGIS_MICRO_VERSION "${_POSTGIS_VERSION_CONFIG}")
  
set(POSTGIS_VERSION "${POSTGIS_MAJOR_VERSION}.${POSTGIS_MINOR_VERSION} USE_GEOS=${POSTGIS_GEOS_VERSION} USE_PROJ=${POSTGIS_PROJ_VERSION} USE_STATS=${POSTGIS_USE_STATS}")
  set(POSTGIS_LIB_VERSION "${POSTGIS_MAJOR_VERSION}.${POSTGIS_MINOR_VERSION}.${POSTGIS_MICRO_VERSION}")
  set(POSTGIS_SCRIPTS_VERSION "${POSTGIS_LIB_VERSION}")

  unset(_VERSION_CONFIG_CONTENTS)
endif()

message("Postgis library ${POSTGIS_LIB_VERSION}")

set(POSTGIS_LIBRARY postgis-${POSTGIS_LIB_VERSION})

# Determine build date and time
if (WIN32)
	string(TIMESTAMP POSTGIS_BUILD_DATE)
elseif(UNIX)
    exec_program(date ARGS -u "\"+%Y-%m-%d %H:%M:%S\""
        OUTPUT_VARIABLE POSTGIS_BUILD_DATE)
endif()

# Determine revision
message(STATUS "Generating revision header ${CMAKE_SOURCE_DIR}/postgis_revision.h")
find_package(Perl)
if (PERL_FOUND)
  execute_process(COMMAND sh -c 
    "cd ${CMAKE_SOURCE_DIR} && ${PERL_EXECUTABLE} utils/repo_revision.pl")

  file(COPY "${CMAKE_SOURCE_DIR}/postgis_revision.h"
    DESTINATION "${CMAKE_BINARY_DIR}")
else()
  message("*** Perl not found, cannot create postgis_revision.h")
  message("*** Check revision and create revision header manually:")
  message("*** echo '#define POSTGIS_REVISION XYZ' > ${CMAKE_SOURCE_DIR}/postgis_revision.h")
endif()

################################################################################
# Predefined constants
set(SRID_MAX 999999)
set(SRID_USR_MAX 998999)

################################################################################
# Generate config header
configure_file("postgis_config.h.cmake"
  "${CMAKE_CURRENT_BINARY_DIR}/postgis_config.h")
include_directories(${CMAKE_CURRENT_BINARY_DIR})

################################################################################
# Configure all subdirectories with any build targets
### Object libraries
add_subdirectory(liblwgeom)

get_property(LIBLWGEOM_INCLUDE_DIRS GLOBAL PROPERTY LIBLWGEOM_INCLUDE_DIRS)
include_directories(${LIBLWGEOM_INCLUDE_DIRS})

add_subdirectory(libpgcommon)

get_property(LIBPGCOMMON_INCLUDE_DIRS GLOBAL PROPERTY LIBPGCOMMON_INCLUDE_DIRS)
include_directories(${LIBPGCOMMON_INCLUDE_DIRS})

### Shared libraries

add_subdirectory(postgis)

add_subdirectory(extensions)

add_subdirectory(loader)

################################################################################
# Install

# TODO

################################################################################
# Configuration summary
message(STATUS)
postgis_report_value(POSTGIS_VERSION)
postgis_report_value(POSTGIS_LIB_VERSION)
postgis_report_value(POSTGIS_SCRIPTS_VERSION)
postgis_report_value(POSTGIS_BUILD_DATE)
postgis_report_value(POSTGIS_WITH_TOPOLOGY)
postgis_report_value(POSTGIS_WITH_RASTER)
postgis_report_value(POSTGIS_WITH_TOPOLOGY)
postgis_report_value(POSTGIS_PGSQL_VERSION)
