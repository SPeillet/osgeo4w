--- "loader\\shpcommon.c.orig"	2019-02-20 15:15:49.050705300 +0100
+++ "loader\\shpcommon.c"	2019-02-20 15:27:47.912466800 +0100
@@ -13,6 +13,8 @@
 
 /* This file contains functions that are shared between the loader and dumper */
 
+#include "asprintf.h"
+
 #include <stdio.h>
 #include <string.h>
 #include <stdlib.h>
