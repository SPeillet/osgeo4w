--- postgis/gserialized_spgist_3d.c
+++ postgis/gserialized_spgist_3d.c
@@ -369,7 +369,7 @@ overBack6D(CubeBox3D *cube_box, BOX3D *query)
 
 PG_FUNCTION_INFO_V1(gserialized_spgist_config_3d);
 
-PGDLLEXPORT Datum gserialized_spgist_config_3d(PG_FUNCTION_ARGS)
+Datum gserialized_spgist_config_3d(PG_FUNCTION_ARGS)
 {
 	spgConfigOut *cfg = (spgConfigOut *)PG_GETARG_POINTER(1);
 
@@ -389,7 +389,7 @@ PGDLLEXPORT Datum gserialized_spgist_config_3d(PG_FUNCTION_ARGS)
 
 PG_FUNCTION_INFO_V1(gserialized_spgist_choose_3d);
 
-PGDLLEXPORT Datum gserialized_spgist_choose_3d(PG_FUNCTION_ARGS)
+Datum gserialized_spgist_choose_3d(PG_FUNCTION_ARGS)
 {
 	spgChooseIn *in = (spgChooseIn *)PG_GETARG_POINTER(0);
 	spgChooseOut *out = (spgChooseOut *)PG_GETARG_POINTER(1);
@@ -414,7 +414,7 @@ PGDLLEXPORT Datum gserialized_spgist_choose_3d(PG_FUNCTION_ARGS)
  */
 PG_FUNCTION_INFO_V1(gserialized_spgist_picksplit_3d);
 
-PGDLLEXPORT Datum gserialized_spgist_picksplit_3d(PG_FUNCTION_ARGS)
+Datum gserialized_spgist_picksplit_3d(PG_FUNCTION_ARGS)
 {
 	spgPickSplitIn *in = (spgPickSplitIn *)PG_GETARG_POINTER(0);
 	spgPickSplitOut *out = (spgPickSplitOut *)PG_GETARG_POINTER(1);
@@ -499,7 +499,7 @@ PGDLLEXPORT Datum gserialized_spgist_picksplit_3d(PG_FUNCTION_ARGS)
  */
 PG_FUNCTION_INFO_V1(gserialized_spgist_inner_consistent_3d);
 
-PGDLLEXPORT Datum gserialized_spgist_inner_consistent_3d(PG_FUNCTION_ARGS)
+Datum gserialized_spgist_inner_consistent_3d(PG_FUNCTION_ARGS)
 {
 	spgInnerConsistentIn *in = (spgInnerConsistentIn *)PG_GETARG_POINTER(0);
 	spgInnerConsistentOut *out = (spgInnerConsistentOut *)PG_GETARG_POINTER(1);
@@ -663,7 +663,7 @@ PGDLLEXPORT Datum gserialized_spgist_inner_consistent_3d(PG_FUNCTION_ARGS)
  */
 PG_FUNCTION_INFO_V1(gserialized_spgist_leaf_consistent_3d);
 
-PGDLLEXPORT Datum gserialized_spgist_leaf_consistent_3d(PG_FUNCTION_ARGS)
+Datum gserialized_spgist_leaf_consistent_3d(PG_FUNCTION_ARGS)
 {
 	spgLeafConsistentIn *in = (spgLeafConsistentIn *)PG_GETARG_POINTER(0);
 	spgLeafConsistentOut *out = (spgLeafConsistentOut *)PG_GETARG_POINTER(1);
@@ -764,7 +764,7 @@ PGDLLEXPORT Datum gserialized_spgist_leaf_consistent_3d(PG_FUNCTION_ARGS)
 
 PG_FUNCTION_INFO_V1(gserialized_spgist_compress_3d);
 
-PGDLLEXPORT Datum gserialized_spgist_compress_3d(PG_FUNCTION_ARGS)
+Datum gserialized_spgist_compress_3d(PG_FUNCTION_ARGS)
 {
 	BOX3D *result = DatumGetBox3DP(DirectFunctionCall1(LWGEOM_to_BOX3D, PG_GETARG_DATUM(0)));
 
