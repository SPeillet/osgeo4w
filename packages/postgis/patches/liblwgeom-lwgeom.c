--- "liblwgeom\\lwgeom.c.orig"	2019-02-20 13:58:41.251288100 +0100
+++ "liblwgeom\\lwgeom.c"	2019-02-20 14:16:58.818641800 +0100
@@ -31,6 +31,7 @@
 #include "liblwgeom_internal.h"
 #include "lwgeom_log.h"
 
+#define out_stack_size 32
 
 /** Force Right-hand-rule on LWGEOM polygons **/
 void
@@ -1606,7 +1607,7 @@ lwgeom_remove_repeated_points_in_place(LWGEOM *geom, double tolerance)
 		}
 		case MULTIPOINTTYPE:
 		{
-			static uint32_t out_stack_size = 32;
+			//static uint32_t out_stack_size = 32;
 			double tolsq = tolerance*tolerance;
 			uint32_t i, j, n = 0;
 			LWMPOINT *mpt = (LWMPOINT *)(geom);
