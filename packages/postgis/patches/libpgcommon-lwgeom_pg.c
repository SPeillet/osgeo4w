--- "libpgcommon\\lwgeom_pg.c.orig"
+++ "libpgcommon\\lwgeom_pg.c"
@@ -40,12 +40,16 @@ postgisConstants *POSTGIS_CONSTANTS = NULL;
 /* Utility call to lookup type oid given name and nspoid */
 static Oid TypenameNspGetTypid(const char *typname, Oid nsp_oid)
 {
-	return GetSysCacheOid2(TYPENAMENSP,
 #if POSTGIS_PGSQL_VERSION >= 120
+	return GetSysCacheOid2(TYPENAMENSP,
 	                       Anum_pg_type_oid,
-#endif
 	                       PointerGetDatum(typname),
 	                       ObjectIdGetDatum(nsp_oid));
+#else
+	return GetSysCacheOid2(TYPENAMENSP,
+	                       PointerGetDatum(typname),
+	                       ObjectIdGetDatum(nsp_oid));
+#endif
 }
 
 /* Cache type lookups in per-session location */