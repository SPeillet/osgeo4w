::--------- Package settings --------
:: package name
set P=python3-colour
:: version
set V=0.1.5
:: package version
set B=1

:: pip name
set N=colour

set HERE=%CD%

::--------- Prepare the environment
call ..\__inc__\prepare_env.bat %1
set OSGEO4W_ROOT=c:\osgeo4w64
set PATH=%OSGEO4W_ROOT%\bin;%PATH%

:: python3 package
call %OSGEO4W_ROOT%\bin\py3_env.bat

python3 -m pip install %N%==%V% || goto :error

tar -C %OSGEO4W_ROOT% -cvjf %PKG_BIN% apps/Python37/Lib/site-packages/%N%.py apps/Python37/Lib/site-packages/%N%-%V%.dist-info || goto :error

::--------- Installation
scp %PKG_BIN% %R% || goto :error
cd %HERE%
scp setup.hint %R% || goto :error

goto :EOF

:error
echo Build failed
exit /b 1
