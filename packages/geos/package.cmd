::--------- Package settings --------
:: package name
set P=geos
:: version
set V=3.8.1
:: package version
set B=1

set HERE=%CD%

::--------- Prepare the environment
call ..\__inc__\prepare_env.bat %1

wget --progress=bar:force https://download.osgeo.org/geos/geos-%V%.tar.bz2  || goto :err

tar xjf geos-%V%.tar.bz2 || goto :error

mkdir install
cd geos-%V%

mkdir build
cd build

cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo ^
	  -DCMAKE_INSTALL_PREFIX=%HERE%/install ^
      -G "NMake Makefiles" .. || goto :error

cmake --build . --target install VERBOSE=1 || goto :error

cd %HERE%

c:\cygwin64\bin\tar.exe -C install -cjvf %PKG_BIN% lib bin include || goto :error

::--------- Installation
call %HERE%\..\__inc__\install_archives.bat || goto :error

goto :EOF

:error
echo Build failed
exit /b 1
