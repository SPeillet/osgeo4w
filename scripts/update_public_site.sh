#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

ftp="osgeowosid@ftp.cluster023.hosting.ovh.net"
mirror_ftp="/mnt/osgeo4w_ftp/www/mirror/"

log_file=/home/osgeo4w/osgeo4w-update.log

exec &> >(tee -a $log_file)

# if there is already another synchronisation running, we wait
exec 100>/var/tmp/testlock.lock || exit 1
flock 100 || exit 1

echo "##### Start mirror synchronisation $(date) ..."

echo ----------- MIRROR -----------
$DIR/mirror-osgeo4w.sh

cd /home/osgeo4w

echo
echo ----------- EXTRA -----------
ssh $ftp 'rsync -av --delete --exclude="x86_64/release/extra/*" www/mirror/ www/extra/'
echo gen_setup_ini ...
$DIR/gen_setup_ini.sh release
echo
echo ----------- EXTRA test -----------
ssh $ftp 'rsync -av --delete --exclude="x86_64/release/extra/*" www/mirror/ www/extra.test/'
echo gen_setup_ini ...
$DIR/gen_setup_ini.sh test    

# rotating logs
if [ $(wc -c <"$log_file") -ge 1000000 ]; then
    savelog -n -l -c 7 $log_file
fi

echo "##### End of mirror synchronisation $(date) ..."

